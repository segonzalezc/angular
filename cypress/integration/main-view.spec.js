const { createYield } = require("typescript")

describe('Ventana principal', () =>{
  it('Da la opcion de ingresar al sistema' , () =>{
    cy.visit('http://localhost:4200');
    cy.get('p.ng-star-inserted').should('contain','Ingresar');
  })

  it('Ingresa nueva habilidad', () =>{
    cy.visit('http://localhost:4200/');
    cy.get('p.ng-star-inserted').click();
    cy.get('a.Admin').click();
    cy.get("input#inputLenguaje").type("C++");
    cy.get("input#inputHoras").type("6");
    cy.get("button#enviar").click();
    cy.contains("td", "C++");
  })

  it('Valida no ingreso de horas', () =>{
    cy.visit('http://localhost:4200/');
    cy.get('p.ng-star-inserted').click();
    cy.get('a.Admin').click();
    cy.get("input#inputLenguaje").type("C++");
    cy.get("input#inputHoras").focus();
    cy.get("input#inputLenguaje").focus();
    cy.contains("div", "Cantidad horas requerida");
  })

});


