export class HabilidadModule {
  Lenguaje: string;
  Horas: number;
  Voto: number;

  constructor(Lenguaje: string, Horas: number) {
    this.Lenguaje = Lenguaje;
    this.Horas = Horas;
    this.Voto = 0;
  }
}

export function InitializeHabilidad(): HabilidadModule[] {
  return ( [] );
}
