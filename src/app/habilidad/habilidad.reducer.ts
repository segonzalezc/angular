import { AGREGAR, actions, ELIMINAR, VOTAR, RESET, INIT } from './habilidad.actions';
import { Injectable } from '@angular/core';


export function habilidadReducer(state = [], action: actions) {
  switch (action.type) {


    case RESET:
      return [];

    case AGREGAR:
      return [...state, action.payload];

    case ELIMINAR:
      var newState = [...state];
      newState.splice(action.payload, 1);
      return newState;

    case VOTAR:


      var newStates = state.map(function (habilidad, index) {
        const container = {Lenguaje:'', Horas:0 , Voto: 0};
        if (index === action.payload){
          container.Lenguaje = habilidad.Lenguaje;
          container.Horas = habilidad.Horas;
          container.Voto = habilidad.Voto + 1;
          return container;}

        else
          return habilidad;
      });

      return newStates;

    case INIT:
        return action.payload;

    default:
      return state;
  }
}
