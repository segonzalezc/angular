import { Action } from '@ngrx/store';
import { HabilidadModule } from './habilidad.module';

export const AGREGAR = '[Habilidad] Agregar';
export const ELIMINAR = '[Habilidad] Eliminar';
export const VOTAR = '[Habilidad] Votar';
export const RESET = '[Habilidad] Reset';
export const INIT = '[Habilidad] Inicializacion';


export class AgregarAction implements Action {
  readonly type = AGREGAR;

  constructor(public payload: HabilidadModule) {}
}

export class InicializarAction implements Action {
  readonly type = INIT;

  constructor(public payload: HabilidadModule[]) {}
}

export class EliminarAction implements Action {
  readonly type = ELIMINAR;

  constructor(public payload: number) {}
}

export class VotarAction implements Action {
  readonly type = VOTAR;
  constructor(public payload: number) {}
}

export class ResetAction implements Action  {
  readonly type = RESET;
}

export type actions = AgregarAction | EliminarAction | VotarAction | ResetAction | InicializarAction;
