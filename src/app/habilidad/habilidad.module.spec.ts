import  {AgregarAction, EliminarAction, VotarAction, ResetAction, InicializarAction }   from './habilidad.actions';
import { habilidadReducer } from './habilidad.reducer';
import { HabilidadModule, InitializeHabilidad } from './habilidad.module';

const Ini: HabilidadModule[] = [new HabilidadModule('VB6',2), new HabilidadModule('C++',4)];
describe('ReducerHabilidad', () => {


    it('Debe agregar una habilidad', () => {
      const prevState: HabilidadModule[] = InitializeHabilidad();
      const action: InicializarAction = new InicializarAction(Ini);
      const newState: HabilidadModule[] = habilidadReducer(prevState, action);

      expect(newState.length).toEqual(2);
      expect(newState[1].Lenguaje).toEqual('C++');
    });


    it('Debe agregar una habilidad', () => {
      const prevState: HabilidadModule[] = InitializeHabilidad();
      const action: AgregarAction = new AgregarAction(new HabilidadModule('VB6', 6));
      const newState: HabilidadModule[] = habilidadReducer(prevState, action);

      expect(newState.length).toEqual(1);
      expect(newState[0].Lenguaje).toEqual('VB6');
    });

    it('Debe votar por una habilidad', () => {
      let prevState: HabilidadModule[] = InitializeHabilidad();
      const auxaction: InicializarAction = new InicializarAction(Ini);
      prevState = habilidadReducer(prevState, auxaction);

      const action: VotarAction = new VotarAction(1);
      const newState: HabilidadModule[] = habilidadReducer(prevState,action);

      expect(newState.length).toEqual(2);
      expect(newState[1].Voto).toEqual(1);
    });

    it('Debe eliminar por una habilidad', () => {
      let prevState: HabilidadModule[] = InitializeHabilidad();
      const auxaction: InicializarAction = new InicializarAction(Ini);
      prevState = habilidadReducer(prevState, auxaction);

      const action: EliminarAction = new EliminarAction(0);
      const newState: HabilidadModule[] = habilidadReducer(prevState,action);

      expect(newState.length).toEqual(1);
      expect(newState[0].Lenguaje).toEqual('C++');
    });

    it('Debe Resetear Listado', () => {
      let prevState: HabilidadModule[] = InitializeHabilidad();
      const auxaction: InicializarAction = new InicializarAction(Ini);
      prevState = habilidadReducer(prevState, auxaction);

      const action: ResetAction = new ResetAction();
      const newState: HabilidadModule[] = habilidadReducer(prevState,action);

      expect(newState.length).toEqual(0);
    });

});

