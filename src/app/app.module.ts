import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormHabilidadesComponent } from './form-habilidades/form-habilidades.component';
import { ListHabilidadesComponent } from './list-habilidades/list-habilidades.component';
import { habilidadReducer } from './habilidad/habilidad.reducer';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import {
  AutentificacionService,
  TEXT_LOGIN,
  TEXT_LOGOUT,
} from './services/autentificacion.service';
import { FooterService } from './services/footer.service';
import { CopyrightService } from './services/copyright.service';
import { GNUService } from './services/gnu.service';
import { FooterComponent } from './footer/footer.component';
import { PublicoComponent } from './publico/publico.component';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import {  DexieService  } from './core/dexie.service';
import { SpyDirective } from './spy.directive';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FormHabilidadesComponent,
    ListHabilidadesComponent,
    FooterComponent,
    PublicoComponent,
    SpyDirective,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ habilidades: habilidadReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  providers: [
    GNUService,
    ApiService,
    CopyrightService,
    AutentificacionService,
    FooterService,
    DexieService,

    { provide: TEXT_LOGIN, useValue: 'Ingresar' },
    { provide: TEXT_LOGOUT, useValue: 'Salir' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
