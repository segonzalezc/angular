import { Directive, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appSpy]',
})
export class SpyDirective implements OnInit, OnDestroy {
  static nextId = 0;
  private element: HTMLElement;


  constructor(private elRef: ElementRef) {
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe((evento) => this.track(evento));
  }

  log(msg: string): void {
    console.log('Evento ' + SpyDirective.nextId++ + ' ' + msg);
  }

  track(evento: Event): void {
    const elemTags = this.element.attributes
      .getNamedItem('data-trackear-tags')
      .value.split(' ');
    this.log('||||||||||||| track evento: ' + elemTags);
  }

  ngOnInit() {
    this.log('########******** onInnit');
  }
  ngOnDestroy() {
    this.log('########******** ondestroy');
  }
}
