import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cantidadHabilidades: number;
  title = 'Listado de Habilidades';


  procesaNuevaHabilidad(event: number){
    this.cantidadHabilidades = event;
  }
}
