import { Injectable } from '@angular/core';
import { HabilidadModule } from '../habilidad/habilidad.module';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private Habilidad: HabilidadModule;
  private URL = "http://localhost:3000/lenguajes";
  constructor(private http: HttpClient) { }


  public GetLenguajes(): Observable<HabilidadModule[]> {
    return this.http.get<HabilidadModule[]>(this.URL);
  }


  public PostLenguajes(listaHabilidades: HabilidadModule): Observable<HabilidadModule> {
  let httpHeaders = new HttpHeaders({
    'Content-Type' : 'application/json',
    'Cache-Control': 'no-cache'
       });
       let options = {
    headers: httpHeaders
       };
       return this.http.post<HabilidadModule>(this.URL, listaHabilidades, options);
      }
}
