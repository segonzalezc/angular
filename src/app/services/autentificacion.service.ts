import { Injectable } from '@angular/core';
import {  Observable, of as observableOf } from 'rxjs';
import { InjectionToken } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AutentificacionService {

  private Logeado: boolean = false;
  public LoginUsuario$ : Observable<boolean>;


  Logear(){
    this.Logeado = true;

  }

  Deslogear(){
    this.Logeado = false
  }

  constructor() {

   }

   UsuarioAutentificado(): Observable<boolean> {

     return observableOf(this.Logeado);
  }

  EstadoLogin():boolean{
    return this.Logeado;
  }
}


export const TEXT_LOGIN= new InjectionToken<string>('');
export const TEXT_LOGOUT= new InjectionToken<string>('');
