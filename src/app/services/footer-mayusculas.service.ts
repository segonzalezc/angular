import { Injectable } from '@angular/core';
import { FooterService } from './footer.service';

@Injectable({
  providedIn: 'root'
})
export class FooterMayusculasService extends FooterService {

   nombreCompania = 'CLINICA ALEMANA';
   creadoPor = 'SERGIO GONZALEZ';

}
