import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FooterService {
  constructor() {}

  nombreCompania = 'Clinica Alemana';
  creadoPor = 'Sergio Gonzalez';
  getNombreCompania(): string {
    return this.nombreCompania;
  }
  getCreadoPor(): string {
    return this.creadoPor;
  }
}
