import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CopyrightService {

  _derechos: string
  constructor() {
    this._derechos = 'Todos los derechos reservados Clinica Alemana 2019-2020';
  }

  getDerechos(): string{
    return this._derechos;
  }
}
