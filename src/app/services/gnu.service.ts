import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GNUService {
  _derechos: string;

  constructor() {
    this._derechos = 'Ud es libre de estudiar, modificar y distribuir este software'
   }

  getDerechos(): string{
    return this._derechos;
  }
}
