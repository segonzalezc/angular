import { Component, OnInit } from '@angular/core';
import { FooterService } from '../services/footer.service';
import { FooterMayusculasService } from '../services/footer-mayusculas.service'
import {  GNUService } from '../services/gnu.service'
import { CopyrightService } from '../services/copyright.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers:[{provide: FooterService, useClass: FooterMayusculasService },
             {provide: CopyrightService, useExisting: GNUService }]

})
export class FooterComponent implements OnInit {

  public compania: string;
  public quien: string;
  public derechos: string
  constructor(private service: FooterService, private servicecopy : CopyrightService) {
    this.compania = service.getNombreCompania();
    this.quien = service.getCreadoPor();
    this.derechos = servicecopy.getDerechos();
  }

  ngOnInit(): void {
  }

}
