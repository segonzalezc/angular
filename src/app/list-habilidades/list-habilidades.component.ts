import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { HabilidadModule } from '../habilidad/habilidad.module';
import {
  EliminarAction,
  VotarAction,
  AgregarAction,
  InicializarAction,
} from '../habilidad/habilidad.actions';
import { ApiService } from '../services/api.service';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';

@Component({
  selector: 'app-list-habilidades',
  templateUrl: './list-habilidades.component.html',
  styleUrls: ['./list-habilidades.component.css'],
  animations: [
    trigger('esVotado', [
      state("cantidadImpar", style({ background: "green" })),
      state("cantidadPar", style({ background: "grey" })),
      transition("void => *", animate(500)),
      transition("cantidadImpar => cantidadPar", animate(500)),
      transition("cantidadPar => cantidadImpar", animate(500))

    ]),
  ],
})
export class ListHabilidadesComponent implements OnInit {
  listaActualizada: HabilidadModule[] = [];
  @Output() NuevaHabilidadAdquirida = new EventEmitter<number>();

  constructor(
    private store: Store<{ habilidades: HabilidadModule[] }>,
    private apiLenguaje: ApiService
  ) {
    this.store.select('habilidades').subscribe((lista) => {
      this.listaActualizada = lista;
      this.NuevaHabilidadAdquirida.emit(this.listaActualizada.length);
    });
  }

  ngOnInit(): void {
    let listaHTTP: HabilidadModule[] = [];
    this.apiLenguaje.GetLenguajes().subscribe((Lista) => {
        const accion = new InicializarAction(Lista);
        this.store.dispatch(accion);
    });
  }

  removeHabilidad(event: number) {
    const accion = new EliminarAction(event);
    this.store.dispatch(accion);
  }

  AgregarVoto(event: number) {
    const accion = new VotarAction(event);
    this.store.dispatch(accion);
  }

  esVotadoPar(i: number): boolean {
    if (this.listaActualizada[i].Voto % 2 == 0) return true;
    return false;
  }
}
