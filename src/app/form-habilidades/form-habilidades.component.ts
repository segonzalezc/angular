import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';

import { HabilidadModule } from '../habilidad/habilidad.module';
import { Store } from '@ngrx/store';
import { AgregarAction } from '../habilidad/habilidad.actions';
import { ApiService } from '../services/api.service'
import { BD } from '../core/dexie.service'

export function ValidaHoras(control: AbstractControl) {
  if (control.value <= 0) {
    return { horasvalidas: true };
  }
  return null;
}

@Component({
  selector: 'app-form-habilidades',
  templateUrl: './form-habilidades.component.html',
  styleUrls: ['./form-habilidades.component.css'],
})




export class FormHabilidadesComponent implements OnInit {
  habilidadesAdquiridas: Array<HabilidadModule> = [];

  habilidadForm = new FormGroup({
    lenguaje: new FormControl(''),
    horas: new FormControl(''),
  });

  constructor(
    private fb: FormBuilder,
    private apiservice: ApiService,

    private store: Store<{ habilidades: HabilidadModule[]  }>
  ) {}

  ngOnInit(): void {
    this.habilidadForm = new FormGroup({
      lenguaje: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
      ]),
      horas: new FormControl('', [Validators.required, ValidaHoras]),
    });
  }

  onSubmit() {

    const NuevaHabilidad = new HabilidadModule (this.habilidadForm.value.lenguaje, this.habilidadForm.value.horas);
    this.apiservice.PostLenguajes(NuevaHabilidad).subscribe(resp => {
      const myBd = BD;
      myBd.Agregar(resp);
      const accion = new AgregarAction(resp);
      this.store.dispatch(accion);
    })

  }


}
