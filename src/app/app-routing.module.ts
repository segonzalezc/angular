import { Routes, RouterModule } from '@angular/router';
import {  FormHabilidadesComponent  } from './form-habilidades/form-habilidades.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth.guard'
import {  PublicoComponent } from './publico/publico.component';
import { MapaComponent } from './mapa/mapa.component'


const routes: Routes = [
  {
    path: '',
    component: PublicoComponent,
  },
  {
    path: 'admin',
    component: FormHabilidadesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'mapa',
    component: MapaComponent
  },
  {
    path: '**',
    redirectTo: '',
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
