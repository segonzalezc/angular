import Dexie from 'dexie';
import { Injectable } from '@angular/core';
import { HabilidadModule } from '../habilidad/habilidad.module';

@Injectable({
  providedIn: 'root'
})
export class DexieService extends Dexie {
  tabla: Dexie.Table<HabilidadModule, number>;

  constructor() {
    super('MyBdEjemplo');
    this.version(1).stores({
      tabla: "++id, Lenguaje, Horas, Votos",
    });
  }

  Agregar(NuevaHabilidad: HabilidadModule ){
    return this.tabla.add(NuevaHabilidad);
  }

}

export const BD = new DexieService();
