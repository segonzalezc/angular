import { Component, OnInit, Input, Inject } from '@angular/core';
import {
  AutentificacionService,
  TEXT_LOGIN,
  TEXT_LOGOUT,
} from '../services/autentificacion.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})

export class NavbarComponent implements OnInit {
  @Input() cantidadAprendida: number;
  public textoIngresar: string;
  public textoSalir: string;

  cantidadHabilidades: number;
  constructor(
    private servicio: AutentificacionService,
    @Inject(TEXT_LOGIN) private textLogin: string,
    @Inject(TEXT_LOGOUT) private textLogout: string,
    private router: Router
  ) {
    this.textoIngresar = textLogin;
    this.textoSalir = textLogout;
  }

  ngOnInit(): void {
    this.cantidadAprendida = 0;
  }

  onLogear() {
    this.servicio.Logear();
    console.log('Login');
  }

  onDeslogear() {
    this.servicio.Deslogear();
    this.router.navigate(['/']);
    console.log('Logout');
  }

  estadoLogin(): boolean {
    return this.servicio.EstadoLogin();
  }
}
